'''
This script is created to use in all scripts and notebooks
'''

import pickle
import numpy as np

from matplotlib import pyplot as plt
import cv2
import os

import pandas as pd
import matplotlib.image as mpimg
from keras.utils.np_utils import to_categorical
from sklearn.model_selection import train_test_split


from keras.utils.np_utils import to_categorical # convert to one-hot-encoding
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D
from keras.optimizers import RMSprop, Adam
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ReduceLROnPlateau, EarlyStopping, ModelCheckpoint
from keras import optimizers


# Create our shapening kernel, it must equal to one eventually
kernel_sharpening = np.array([[-1,-1,-1],
                              [-1, 9,-1],
                              [-1,-1,-1]])

# Taking a matrix of size 5 as the kernel
kernel = np.ones((5,5), np.uint8)

img_dims = (100, 100)

DEBUG_MOD = True # False



def plot_histograms(img: np.ndarray, denoised_img: np.ndarray):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))
    ax1.hist(img[:, :, 2].ravel(), 256, [0, 256])
    ax2.hist(denoised_img[:, :, 2].ravel(), 256, [0, 256])
    plt.show()


def switch_channel(in_img: np.ndarray):
    b, g, r = cv2.split(in_img)  # get b,g,r
    in_img = cv2.merge([g, b, r])  # switch it to rgb
    #hsv_img = cv2.cvtColor(in_img, cv2.COLOR_BGR2HSV)
    #hsv_img[:, :, 0] = (hsv_img[:, :, 0] * 0.9).astype(np.uint8)
    return in_img #cv2.cvtColor(hsv_img, cv2.COLOR_HSV2BGR)


def denoise_and_sharpen_image(in_img: np.ndarray):
    denoised_img = cv2.fastNlMeansDenoisingColored(in_img, None, 12, 12, 7, 33)
    sharpen_img = cv2.filter2D(cv2.blur(denoised_img,(3, 3)) , -1, kernel_sharpening) #
    return sharpen_img


def calculate_fourier(in_img: np.ndarray):

    im_gray = cv2.cvtColor(in_img, cv2.COLOR_BGR2GRAY) if len(in_img.shape) > 2 else in_img.copy()

    dft = cv2.dft(np.float32(im_gray), flags=cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)
    return cv2.magnitude(dft_shift[:, :, 0], dft_shift[:, :, 1])


def apply_low_pass_filter(in_img: np.ndarray, in_window_size: int = 30):
    dft_shift = calculate_fourier(in_img)

    rows, cols = in_img.shape[:2]
    center_x, center_y = int(rows / 2), int(cols / 2)
    mask = np.zeros((rows, cols), np.uint8)
    mask[center_x - in_window_size:center_x + in_window_size, center_y - in_window_size:center_y + in_window_size] = 1
    '''filter out high freq'''
    dft_shift = dft_shift * mask
    f_ishift = np.fft.ifftshift(dft_shift)
    img_back = np.fft.ifft2(f_ishift)
    img_back = np.abs(img_back)

    return img_back / np.max(img_back)


def calc_fourier_power_spec(in_img: np.ndarray):

    dft_shift = calculate_fourier(in_img)
    magnitude_spectrum = 20 * np.log(dft_shift)
    return magnitude_spectrum


def save_file(file_path: str, file: list):
    with open(file_path, "wb") as fp:  # Pickling
        pickle.dump(file, fp)


def load_file(file_path: str):
    with open(file_path, "rb") as fp:  # Pickling
        file = pickle.load(fp)
    return file

def getListOfFiles( dirName : str):
    # create a list of file and sub directories 
    # names in the given directory 
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
            
    return allFiles 

"""plot distribtuion of labels"""
def plot_dist( labels : dict):
    df_labels = pd.DataFrame.from_dict(labels)
    df_labels = df_labels.iloc[0]

    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    ax.bar(df_labels.index,df_labels)
    plt.show()

def write_data(x_img, y_lbl, images_file_name, labels_file_name):
    f_img = open(images_file_name, 'wb')
    f_lbl = open(labels_file_name, 'wb')
    pickle.dump(x_img, f_img, pickle.HIGHEST_PROTOCOL)
    pickle.dump(y_lbl, f_lbl, pickle.HIGHEST_PROTOCOL)
    f_lbl.close()
    f_img.close()

def read_data(images_file_name, labels_file_name):
    f_img = open(images_file_name, 'rb')
    f_lbl = open(labels_file_name, 'rb')
    x_img = pickle.load(f_img)
    y_lbl = pickle.load(f_lbl)
    f_lbl.close()
    f_img.close()
    return x_img, y_lbl

"""
parsing files to process for modelling
"""


def parse_files(data_folder_path, images_file_name, labels_file_name):
    files = getListOfFiles(data_folder_path)
    print("file checking\n", files[:5])
    n_total = len(files)
    img = mpimg.imread(files[0])
    print(np.histogram(img))
    n_channel = 1
    if (len(img.shape)>2):
        n_channel = img.shape[-1]

    '''since conv_2d only accepts 3d array'''
    t_shape = (n_total, *img_dims, n_channel) #
    x_data = np.zeros(t_shape, dtype=float)
    y_data = np.zeros((n_total), dtype='int8')

    labels = {}
    ind_data = 0
    for f1 in files:
        image = None
        if os.path.isfile(f1):
            # load image
            # print(f1)
            img = mpimg.imread(f1)
        else:
            continue
        # copy to draw on
        draw = img.copy()


        # preprocess image for network
        # image = preprocess_image(draw)
        if DEBUG_MOD:
            print(img.dtype)
        if img.dtype == 'uint8':
            # change the dtype to 'float64'
            draw = draw.astype('float64')
            draw = draw / 255
            print("normalize image")
        image =  np.expand_dims(cv2.resize(draw, img_dims), axis=2)

        # register label and images
        class_name = f1.split("-")[-1].split(".")[0][:3]

        print(class_name)

        if class_name in labels:
            labels[class_name][0] += 1
        else:
            labels[class_name] = [1]

        x_data[ind_data] = image
        y_data[ind_data] = int(class_name)
        ind_data += 1

    write_data(x_data, y_data, images_file_name, labels_file_name)

    if DEBUG_MOD:
        print(image.max())
        print("plot the histogram of classes")
        plot_dist(labels)
        print("file:{} with class:{}".format(f1, class_name))
    return x_data, y_data


def seperate_train_validation_data(x_data, y_data):
    (unique, counts) = np.unique(y_data, return_counts=True)
    frequencies = np.asarray((unique, counts)).T

    print("number of data for each class:", frequencies)
    print("unique labels", unique)
    n_class = unique.max()

    # Set the random seed
    random_seed = 2

    X_train, X_val, Y_train, Y_val = train_test_split(x_data, y_data, \
                                                      test_size=0.1, random_state=random_seed, stratify=y_data)
    print("n_class:",n_class)
    if n_class>1:
        Y_train, Y_val = convert_label_into_vector(Y_train, Y_val, n_class)

    return X_train, X_val, Y_train, Y_val, n_class



def set_keras_callback(MODEL_NAME):
    early_stopping = EarlyStopping(monitor='loss', patience=4, \
                                   verbose=1, mode='auto', restore_best_weights=True)

    mcp_save = ModelCheckpoint(MODEL_NAME, save_best_only=True, save_weights_only=True, \
                               monitor='val_accuracy', mode='auto', verbose=1)

    learning_rate_reduction = ReduceLROnPlateau(monitor='loss',
                                                patience=3,
                                                verbose=1,
                                                factor=0.5,
                                                min_lr=0.00001)
    call_backs = [early_stopping, mcp_save, learning_rate_reduction]
    return call_backs


def initialize_cnn_model(img_shape, n_class, MODEL_NAME):

    model = Sequential()

    model.add(Conv2D(filters=4, kernel_size=(5, 5), padding='Same',
                     activation='relu', input_shape=img_shape))
    # if n_class == 1:
    #     model.add(Conv2D(filters=4, kernel_size=(5, 5), padding='Same',
    #                      activation='relu', input_shape=img_shape))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters=8, kernel_size=(3, 3), padding='Same',
                     activation='relu', input_shape=img_shape))
    # if n_class == 1:
    #     model.add(Conv2D(filters=8, kernel_size=(3, 3), padding='Same',
    #                      activation='relu', input_shape=img_shape))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))


    model.add(Flatten())
    #model.add(Dense(32, activation="relu"))
    model.add(Dense(n_class, activation="sigmoid"))

    call_backs = set_keras_callback(MODEL_NAME)

    # Define the optimizer
    optimizer = RMSprop(lr=0.001, rho=0.9, epsilon=1e-08, decay=0.0)
    # multi class prediction use categorical_crossentropy as loss accuracy as metrics

    loss_str = "categorical_crossentropy" if n_class > 1 else "binary_crossentropy"
    print("n_class: ",n_class,"\tloss: ",loss_str)
    model.compile(optimizer="adam", loss=loss_str, metrics=["accuracy"])
    print(model.summary())

    if os.path.isfile(MODEL_NAME):
        print(MODEL_NAME," is loading weights")
        model.load_weights(MODEL_NAME)

    return model, call_backs


def draw_accuracy_loss_plot(history):
    # Plot the loss and accuracy curves for training and validation
    fig, ax = plt.subplots(2,1)
    ax[0].plot(history.history['loss'], color='b', label="Training loss")
    ax[0].plot(history.history['val_loss'], color='r', label="validation loss",axes =ax[0])
    legend = ax[0].legend(loc='best', shadow=True)

    ax[1].plot(history.history['accuracy'], color='b', label="Training accuracy")
    ax[1].plot(history.history['val_accuracy'], color='r',label="Validation accuracy")
    legend = ax[1].legend(loc='best', shadow=True)


import itertools


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.figure(figsize=(10, 10))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")
    # plt.figure(num=None, figsize=(8, 8), dpi=80, facecolor='w', edgecolor='k')
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()


def display_errors(errors_index,img_errors,pred_errors, obs_errors, img_shape):
    """ This function shows 6 images with their predicted and real labels"""
    n = 0
    nrows = 2
    ncols = 3
    fig, ax = plt.subplots(nrows,ncols,sharex=True,sharey=True, figsize=(15,10))
    n_errors = len(errors_index)
    for row in range(nrows):
        for col in range(ncols):
            if n>=n_errors:
                break
            error = errors_index[n]
            ax[row,col].imshow(np.squeeze((img_errors[error]), axis=2).reshape(img_shape), cmap='gray',clim=(0,1))
            ax[row,col].set_title("Predicted label :{}\nTrue label :{}".format(pred_errors[error],obs_errors[error]))
            n += 1

    plt.show()



"""
HYPER PARAMETERS OF CNN
"""
epochs = 100 # Turn epochs to 30 to get 0.9967 accuracy
batch_size = 512


def build_and_fit_cnn_model(X_train, Y_train, X_val,Y_val, n_class, MODEL_NAME):
    global epochs, batch_size
    img_shape = X_train[0].shape
    model, call_backs = initialize_cnn_model(img_shape, n_class, MODEL_NAME)
    model.optimizer.learning_rate.assign(model.optimizer.learning_rate*2)
    print(model.optimizer.learning_rate)
    history = model.fit(X_train,Y_train, batch_size=batch_size,\
          epochs = epochs, validation_data = (X_val,Y_val),\
          verbose = 2, callbacks=call_backs)
    #plot accuracy and loss
    draw_accuracy_loss_plot(history)
    return model


def convert_label_into_vector(y_train, y_val, n_class):
    y_train = to_categorical(y_train-1, num_classes = n_class)
    y_val = to_categorical(y_val-1, num_classes = n_class)
    return y_train, y_val


from sklearn.metrics.pairwise import cosine_similarity


def create_similarity_table(y_true):
    result = []
    n_sample = len(y_true)
    for i in range(n_sample):
        for j in range(i + 1, n_sample):
            result.append([y_true[i], i, y_true[j], j, -1])

    cum_result_df = pd.DataFrame(result, columns=["person1", "img1no", "person2", "img2no", "score"])
    return cum_result_df, n_sample


def calculate_similarity_table(arr, y_true, similarity_table=None, model=None):
    
    if model is None:
        similarities = cosine_similarity(arr, dense_output=False)
        ind_df = 0
        similarity_table, n_sample = create_similarity_table(y_true)

        for i in range(n_sample):
            for j in range(i + 1, n_sample):
                similarity_table.iloc[ind_df,-1] = similarities[i, j]
                ind_df += 1

        similarity_table["score"] = (similarity_table["score"] + 1) / 2

    else:
        y_prob = model.predict(arr)
        print(similarity_table.shape, y_prob.shape)
        similarity_table["score"] = y_prob

    return similarity_table


def similarity_prediction(result_df: pd.DataFrame, threshold: float = 0.5):
    cum_result_df = result_df.copy()
    cum_result_df["pred"] = cum_result_df["score"] >= threshold
    '''cum_result_df["TP"] = (cum_result_df["person1"] == cum_result_df["person2"]) & cum_result_df["is_similar"]
    cum_result_df["FP"] = (cum_result_df["person1"] == cum_result_df["person2"]) & ~cum_result_df["is_similar"]
    cum_result_df["TN"] = (cum_result_df["person1"] != cum_result_df["person2"]) & ~cum_result_df["is_similar"]
    cum_result_df["FN"] = (cum_result_df["person1"] != cum_result_df["person2"]) & cum_result_df["is_similar"]
    cum_result_df["pred"] = cum_result_df["TP"] | cum_result_df["TN"]'''


    return cum_result_df


import sklearn.metrics as metrics


def plot_roc_curve(result_df):
    fpr, tpr, thresholds = metrics.roc_curve(result_df["gr_truth"], result_df["score"])
    roc_auc = metrics.auc(fpr, tpr)

    ####################################
    # The optimal cut off would be where tpr is high and fpr is low
    # tpr - (1-fpr) is zero or near to zero is the optimal cut off point
    ####################################
    i = np.arange(len(tpr))  # index for df
    roc = pd.DataFrame(
        {'fpr': pd.Series(fpr, index=i), 'tpr': pd.Series(tpr, index=i), '1-fpr': pd.Series(1 - fpr, index=i),
         'tf': pd.Series(tpr - (1 - fpr), index=i), 'thresholds': pd.Series(thresholds, index=i)})
    best_thresh = roc.iloc[(roc.tf - 0).abs().argsort()[:1]]



    # Plot tpr vs 1-fpr
    ax = plt.gca()
    plt.plot([0, 1], [0, 1], linestyle='--', label='No Skill')
    # plot model roc curve

    plt.plot(fpr, tpr, marker='.', label='similarity_detector')
    plt.axis('tight')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc = 'lower right')
    ax.set_xticklabels([])

    print("roc_auc: ", roc_auc, "best_thresh", best_thresh["thresholds"].values[0])

    return roc_auc, best_thresh, roc


from sklearn.metrics import precision_recall_curve, f1_score, auc


def plot_precision_recall_curve(result_df):
    # calculate precision-recall curve
    precision, recall, thresholds = precision_recall_curve(result_df["gr_truth"], result_df["score"])
    f1, auc_val = f1_score(result_df["gr_truth"], result_df["pred"]), auc(recall, precision)
    # summarize scores
    print('Logistic: f1=%.3f auc=%.3f' % (f1, auc_val))
    # plot the precision-recall curves
    testy = result_df["gr_truth"].values
    no_skill = len(testy[testy == 1]) / len(testy)
    plt.plot([0, 1], [no_skill, no_skill], linestyle='--', label='No Skill')
    plt.plot(recall, precision, marker='.', label='similarity_detector')
    # axis labels
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    # show the legend
    plt.legend()

    return precision, recall, thresholds


def balance_data(df_sim_tab, y_label):
    print(df_sim_tab["gr_truth"].value_counts())
    sampled_df = pd.DataFrame([], columns=df_sim_tab.columns)
    uniqs = np.unique(y_label)
    for i_person in uniqs:
        df_person = df_sim_tab[df_sim_tab["person1"] == i_person]
        df_similar = df_person[df_person["gr_truth"] == 1]
        n_similar = len(df_similar)
        n_not_similar = len(df_person) - n_similar
        print("i_person: ", i_person, " is processing\n")

        if n_similar < 1:
            #print("there is no positive sample")
            continue
        if n_not_similar > 0:
            df_not_similar = df_person[df_person["gr_truth"] == 0].sample(n_similar + 20)
            #print(df_not_similar.head(5))
        else:
            df_not_similar = pd.DataFrame([], columns=df_sim_tab.columns)
            #print("there is no negative sample")
        sampled_df = pd.concat([sampled_df, df_similar, df_not_similar])
    sampled_df = sampled_df.reset_index(drop=True)
    print(sampled_df["gr_truth"].value_counts())

    return sampled_df


def concatenate_images(sampled_df, x_data):
    n_sample_data = len(sampled_df)
    concat_img = np.zeros((n_sample_data, x_data.shape[1], x_data.shape[2] * 2, x_data.shape[3]))
    y_label = np.zeros(n_sample_data, dtype="int8")
    for i in range(n_sample_data):
        row = sampled_df.iloc[i]
        img1no, img2no = row["img1no"], row["img2no"]
        concat_img[i] = np.concatenate((x_data[img1no], x_data[img2no]), axis=1)
        y_label[i] = row["gr_truth"]

    return concat_img, y_label


from skimage.feature import hog
winStride = (8, 8)
padding = (8, 8)
locations = ((10, 20),)


hog_dict = {}


def calculate_hog_image(img_no, img):
    global hog_dict, hog, winStride, padding, locations
    if img_no in hog_dict:
        hist = hog_dict[img_no]
    else:
        _, hist = hog(img, orientations=9, pixels_per_cell=winStride,
                            cells_per_block=(2, 2), visualize=True)
        hog_dict[img_no] = hist
    return hist


def differ_images(sampled_df, x_data):
    n_sample_data = len(sampled_df)
    dimensions = (100, 100, 1)
    y_label = np.zeros(n_sample_data, dtype="int8")
    concat_img = np.zeros((n_sample_data, *dimensions))

    for i in range(n_sample_data):
        row = sampled_df.iloc[i]
        img1no, img2no = row["img1no"], row["img2no"]

        img1 = (x_data[img1no][:, :, 0] * 255).astype(np.uint8)
        img2 = (x_data[img2no][:, :, 0] * 255).astype(np.uint8)

        img1, img2 = calculate_hog_image(img1no, img1), calculate_hog_image(img2no, img2)

        dif_img = (img1 - img2)**2
        #dif_img = cv2.erode(cv2.erode(dif_img, kernel, iterations=1), kernel, iterations=1)
        #dif_img = cv2.dilate(cv2.dilate(dif_img, kernel, iterations=1), kernel, iterations=1)
        #dif_img = dif_img  # / (np.var(x_data[img1no]) * np.var(x_data[img2no] / np.var(dif_img))+0.0001)
        concat_img[i] = dif_img.reshape(dimensions)  # np.expand_dims(dif_img, axis=2)
        y_label[i] = row["gr_truth"]

    return concat_img, y_label


def fourier_of_images(sampled_df, x_data):
    global kernel
    n_sample_data = len(sampled_df)
    concat_img = np.zeros((n_sample_data, x_data.shape[1], x_data.shape[2], x_data.shape[3]))
    y_label = np.zeros(n_sample_data, dtype="int8")
    for i in range(n_sample_data):
        row = sampled_df.iloc[i]
        img1no, img2no = row["img1no"], row["img2no"]
        mag_spec1 = calc_fourier_power_spec(x_data[img1no][:, :, 0])
        mag_spec2 = calc_fourier_power_spec(x_data[img2no][:, :, 0])
        mag_spec1 = cv2.dilate(cv2.erode(mag_spec1, kernel, iterations=1), kernel, iterations=1)
        mag_spec2 = cv2.dilate(cv2.erode(mag_spec2, kernel, iterations=1), kernel, iterations=1)
        concat_img[i] = np.expand_dims((mag_spec1 - mag_spec2)**2 / (mag_spec1 * mag_spec2), axis=2) #np.expand_dims(np.concatenate((mag_spec1, mag_spec2), axis=1), axis=2) #
        y_label[i] = row["gr_truth"]

    return concat_img, y_label


def calculate_hog_cosine(sampled_df, x_data):
    hog_desc = cv2.HOGDescriptor()
    n_sample_data = len(sampled_df)
    img = (x_data[0][:, :, 0] * 255).astype(np.uint8)
    hist = hog_desc.compute(img, winStride, padding, locations)
    n_hist_sz = len(hist)
    concat_img = np.zeros((n_sample_data, 2*n_hist_sz+1, x_data.shape[3]))
    y_label = np.zeros(n_sample_data, dtype="int8")
    for i in range(n_sample_data):
        row = sampled_df.iloc[i]
        img1no, img2no = row["img1no"], row["img2no"]
        img1 = (x_data[img1no][:, :, 0] * 255).astype(np.uint8)
        img2 = (x_data[img2no][:, :, 0] * 255).astype(np.uint8)

        hist1 = hog_desc.compute(img1, winStride, padding, locations)
        hist2 = hog_desc.compute(img2, winStride, padding, locations)

        cos_sim = cosine_similarity(hist1, hist2, dense_output=False)

        concat_img[i] = np.column_stack((hist1, hist2, cos_sim), axis=0)
        y_label[i] = row["gr_truth"]

    return concat_img, y_label


from xgboost import XGBClassifier
import time


def build_train_xgb(X_train, y_train,X_val, y_val, MODEL_NAME):
    xgb = XGBClassifier(silent=False,
                      scale_pos_weight=1,
                      learning_rate=0.01,
                      colsample_bytree = 0.4,
                      subsample = 0.8,
                      objective='binary:logistic',
                      n_estimators=300,
                      reg_alpha = 0.3,
                      max_depth=4,
                      gamma=10
                        )
    training_start = time.perf_counter()
    xgb.fit(X_train, y_train)
    training_end = time.perf_counter()
    xgb_train_time = training_end - training_start
    print("Time consumed for training: %4.3f" % xgb_train_time)
    save_file(MODEL_NAME, xgb)

    return xgb